package juego;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.Random;

import javax.sound.sampled.Clip;

import entorno.Entorno;
import entorno.Herramientas;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego {

	// El objeto Entorno que controla el tiempo y otros
	private Entorno entorno;
	private Manzana[][] manzanas;
	private Casa[][] casas1,casas2;
	private Casa casaDelPedido;
	private Sakura sakura;
	private Ninja[] ninja;
	private Rasengan rasengan;
	private Image mapa;
	private Image flecha;
	private Image marco;
	private int direccionRasengan;
	private int puntaje;
	private String puntajeMSJ;
	private int ninjasAsesinados;
	private String ninjasAsesinadosMSJ;
	private int[] temporizador;
	private Random aleatorio;
	private int shurikensTemporizador;
	private Shuriken[] shurikens;
	private Moneda[] moneda;
	private int juegoTemp;
	private Clip soundtrack;
	private Clip temaVictoria;
	private Clip temaDerrota;
	private String tiempoMSJ;
	private Image introMarco;
	private Image finalMarco;
	
	public Juego() {
		// Inicializa el objeto entorno
		this.entorno = new Entorno(this, "Sakura ikebana delivery - TP Programación 1 - 1° Semestre 2021", 800, 600);

		// Inicializar lo que haga falta para el juego

		// Crea manzanas
		this.manzanas = new Manzana[4][4];
		int y = entorno.alto() / 10;
		for (int i = 0; i < manzanas.length; i++) {
			int x = entorno.ancho() / 10;
			for (int j = 0; j < manzanas.length; j++) {
				manzanas[i][j] = new Manzana(x, y, entorno.ancho() / 5, entorno.alto() / 5, 0, Color.RED);
				x += entorno.ancho() / 3.7;
			}
			y += entorno.alto() / 3.7;
		}
		
		// Crear Casas 1
		this.casas1 = new Casa[4][4];
		int d = entorno.alto()/8;
		for (int i=0; i<casas1.length; i++) {
			int t = entorno.ancho()/5;
			for (int j=0; j<casas1.length; j++) { 
				if ( i<3) {
					casas1[i][j] = new Casa(manzanas[i][j].getX()+t/4,manzanas[i][j].getY()+d/4,entorno.ancho()/18,entorno.ancho()/18,0,Color.MAGENTA);
				}
				if (i>=3) {
					casas1[i][j] = new Casa(manzanas[i][j].getX()+t/4,manzanas[i][j].getY()-d/4,entorno.ancho()/18,entorno.ancho()/18,0,Color.BLUE);
				}
			}
		}
		
		// Crear Casas 2
		this.casas2 = new Casa[4][4];
		int r = entorno.alto()/8;
		for (int i=0; i<casas2.length;i++) {
			int u = entorno.ancho()/5;
			for (int j=0; j<casas2.length;j++) { 
				if (i==0) {
					casas2[i][j] = new Casa(manzanas[i][j].getX()-u/4,manzanas[i][j].getY()+r/4,entorno.ancho()/18,entorno.ancho()/18,0,Color.WHITE);
				}
				if (i>=1 && i<=2) {
					casas2[i][j] = new Casa(manzanas[i][j].getX()-u/4,manzanas[i][j].getY()-r/4,entorno.ancho()/18,entorno.ancho()/18,0,Color.WHITE);
				}
				if (i>=3) {
					casas2[i][j] = new Casa(manzanas[i][j].getX()-u/4,manzanas[i][j].getY()-r/4,entorno.ancho()/18,entorno.ancho()/18,0,Color.YELLOW);
				}
			}
		}

		// Crear Sakura
		sakura = new Sakura(entorno.ancho() / 2, entorno.alto() / 2, 20, Color.PINK, 2);
		
		// Crear random
		aleatorio = new Random();
		
		// Inicializa direccionRasengan por si sakura lo lanza cuando aun no camino 
		direccionRasengan = 1;

		// Crear ninjas
		ninja = new Ninja[4];
		temporizador = new int[4];
		double w = entorno.ancho()/4.3; // ninjaXinicial = entorno.ancho/4.3 = 186.04...
		double z = entorno.alto()/4.5; // ninjaYinicial = entorno.alto/4.5 = 133.33...
		for (int i=0; i<ninja.length;i++) {
			if (i <2) { // ninjas 0 y 1 verticales
				ninja[i] = new Ninja(w,entorno.alto(),30,1.5);
				w += entorno.ancho()/1.85; // ninjaXsiguiente = entorno.ancho/4.3 + entorno.ancho/1.85
			} 
			if (i>=2) { // ninjas 2 y 3 horizontales
				ninja[i] = new Ninja(entorno.ancho(),z,30,1.5);
				z += entorno.alto()/1.79; // ninjaYsiguiente = entorno.alto/4.5 + entorno.alto/1.79
			}
			temporizador[i] = 0;
		}
		
		// Crear Shurikens
		shurikens = new Shuriken[4];
		for(int i = 0; i < shurikens.length; i++) {
			if(shurikensTemporizador > 0 && ninja[i]!=null) {
				shurikens[i] = new Shuriken(ninja[i].getX(), ninja[i].getY(), 30, 3, i);
				shurikensTemporizador = 0;
			}
		}
		
		// Crear Monedas
		this.moneda = new Moneda[2];
		for (int i=0; i<moneda.length; i++) {
			double f = entorno.ancho()/6 + entorno.ancho()/2 * i;
			double g = entorno.alto()/4.3 + entorno.alto()/1.83 * i;
			if (i<2) {
				moneda[i] = new Moneda(f,g,30,Color.YELLOW);
			}
		}
		
		// Imagenes
		mapa = Herramientas.cargarImagen("finalmapa.jpg");
		marco = Herramientas.cargarImagen("marco.png");
		flecha = Herramientas.cargarImagen("flecha.png");
		introMarco = Herramientas.cargarImagen("introMarco.png");
		finalMarco = Herramientas.cargarImagen("finalMarco.png");
		
		// Canciones
		soundtrack = Herramientas.cargarSonido("soundtrack.wav");
		temaVictoria = Herramientas.cargarSonido("victoria.wav");
		temaDerrota = Herramientas.cargarSonido("derrota.wav");
		soundtrack.loop(1);
		
		// Inicia el juego!
		this.entorno.iniciar();
	}
	
	

	 //* Durante el juego, el método tick() será ejecutado en cada instante y 
	 //* por lo tanto es el método más importante de esta clase. Aquí se debe 
	 //* actualizar el estado interno del juego para simular el paso del tiempo 
	 //* (ver el enunciado del TP para mayor detalle).
	 
	public void tick() {
		// Procesamiento de un instante de tiempo
		if (sakura.getVelocidad()!=0) {
			juegoTemp++;
		}
		
		// Dibujar Manzanas
		for (int i = 0; i < manzanas.length; i++) {
			for (int j = 0; j < manzanas.length; j++) {
				manzanas[i][j].dibujar(entorno);
			}
		}
				
		// Dibujar casas1
		for (int i = 0; i < casas1.length; i++) {
			for (int j = 0; j < casas1.length; j++) {
				casas1[i][j].dibujar(entorno);
			}
		}
		
		// Dibujar casas2
		for (int i = 0; i < casas2.length; i++) {
			for (int j = 0; j < casas2.length; j++) {
				casas2[i][j].dibujar(entorno);
			}
		}
		
		// Dibujar mapa
		entorno.dibujarImagen(mapa,entorno.ancho()/2,entorno.alto()/2,0,1);
		
		// Dibujar Monedas
		if (ninjasAsesinados >= 3) {
			for (int i=0; i<moneda.length; i++) {
				if (moneda[i] != null) {
					moneda[i].dibujar(entorno);
				}
			}
		}
		
		// Actualiza temporizador de reaparición de cada moneda
		for (int i=0; i<moneda.length; i++) {
			if (moneda[i] == null) {
				temporizador[i]++;
			}
		}
		
		// Dibujar NINJAS
		for (int i = 0; i < ninja.length; i++) {
			if (ninja[i] != null) {
				ninja[i].dibujar(entorno);
			}
		}
		
		// Dibujar Shurikens
		for (int i = 0; i < shurikens.length ; i++) {
			if(shurikens[i]!=null) {
				shurikens[i].dibujar(entorno);
			}
		}
	
		// Actualizar Shurikens Temporizador de aparición
		for(int i = 0; i < shurikens.length; i++) {
			if(shurikens[i] == null)
				shurikensTemporizador++;
		}
		
		// Actualiza temporizador de reaparición de cada ninja
		for (int i = 0; i < ninja.length; i++) {
			if (ninja[i] == null) {
				temporizador[i]++;
			}
		}
		
		// Dibujar Sakura
		sakura.dibujar(entorno);
		
		// Dibujar Rasengan y moverlo
		if(rasengan != null) {
			rasengan.dibujar(entorno);
			rasengan.mover();
		}

		// Sakura movimiento y limite  con manzanas y direccion rasengan
		if (entorno.estaPresionada('w') && !entorno.estaPresionada('a') && !entorno.estaPresionada('d')
				&& !sakura.estasChocandoUnaManzanaDesdeAbajo(manzanas)) {
			sakura.moverArriba();
			direccionRasengan = 3;
		}

		if (entorno.estaPresionada('s') && !entorno.estaPresionada('a') && !entorno.estaPresionada('d')
				&& !sakura.estasChocandoUnaManzanaDesdeArriba(manzanas)) {
			sakura.moverAbajo();
			direccionRasengan = 4;
		}

		if (entorno.estaPresionada('a') && !entorno.estaPresionada('w') && !entorno.estaPresionada('s')
				&& !sakura.estasChocandoUnaManzanaDesdeLaDerecha(manzanas)) {
			sakura.moverIzquierda();
			direccionRasengan = 2;
		}

		if (entorno.estaPresionada('d') && !entorno.estaPresionada('s') && !entorno.estaPresionada('w')
				&& !sakura.estasChocandoUnaManzanaDesdeLaIzquierda(manzanas)) {
			sakura.moverDerecha();
			direccionRasengan = 1;
		}
		
		// Sakura limite con entorno
		if (sakura.chocasteConElEntorno(entorno)) {
			sakura.noTeSalgasDelEntorno(entorno);
		}

		// Ninja limite con entorno
		for (int i = 0; i < ninja.length; i++) {
			if (ninja[i] != null) {
				if (ninja[i].chocasteConElEntorno(entorno)) {
					ninja[i].andaAlOtroLado(entorno);
				}
			}
		}

		// Ninja movimiento
		for (int i = 0; i < ninja.length; i++) {
			if (ninja[i] != null) {
				if (i < 2) { // ninjas 0 1 verticales
					if (i % 2 == 0) {
						ninja[i].moverAbajo();
					} else {
						ninja[i].moverArriba();
					}
				}
				if (i >= 2) { // ninjas 2 y 3 horizontales
					if (i % 2 == 0) {
						ninja[i].moverDerecha();
					} else {
						ninja[i].moverIzquierda();
					}
				}
			}
		}
		
		// Sakura muerte / Derrota
		if (sakura.chocasteConUnNinja(ninja) || sakura.tePegóUnShuriken(shurikens)) {
			soundtrack.stop();
			sakura.para();
			temaDerrota.loop(1);
			
			for (int i=0; i < ninja.length; i++) {
				if (ninja[i] != null) {
					ninja[i].stop();
				}
			}
			
			for (int i = 0; i < shurikens.length; i++) {
				if (shurikens[i] !=null) {
					shurikens[i].stop();
				}
			}
			
			for (int i=0; i<moneda.length; i++) {
				if (moneda[i] == null) {
					temporizador[i] = 0;
				}
			}
			entorno.dibujarImagen(finalMarco, entorno.ancho()/2, entorno.alto()/2, 0, 1.5);
			entorno.cambiarFont("Pixel Emulator",60,Color.RED);
			entorno.escribirTexto("Fin del juego:", 120, entorno.alto()/2);
			entorno.cambiarFont("Pixel Emulator",30,Color.RED);
			entorno.escribirTexto("Los ninjas atraparon a Sakura.", 60, entorno.alto()/2 + 50);
		}
		
		// Crear Rasengan
		if (entorno.sePresiono(entorno.TECLA_ESPACIO) && sakura.getVelocidad() != 0) {
			if (rasengan == null) {
				Herramientas.play("rasengan.wav");
				rasengan = new Rasengan(sakura.getX(),sakura.getY(), 15, 4, direccionRasengan);
			}
		}
		
		// Rasengan limite con entorno
		if (rasengan != null) {
			if (rasengan.chocoConElEntorno(entorno)) {
				rasengan = null;
			}
		}
		
		// Rasengan asesinar ninja y contador ninjas asesinados
		if (rasengan != null) {
			if (rasengan.chocoConUnNinja(ninja)) {
				if (rasengan.dameElIDelNinjaChocado(ninja)==0) { // ninjas 0 vertical
					ninja[rasengan.dameElIDelNinjaChocado(ninja)] = null;
				}
				if (rasengan.dameElIDelNinjaChocado(ninja)==1) { // ninjas 1 vertical
					ninja[rasengan.dameElIDelNinjaChocado(ninja)] = null;
				}
				if (rasengan.dameElIDelNinjaChocado(ninja)==2) { // ninjas 2 horizontal
					ninja[rasengan.dameElIDelNinjaChocado(ninja)] = null;
				}
				if (rasengan.dameElIDelNinjaChocado(ninja)==3) { // ninjas 3 horizontal
					ninja[rasengan.dameElIDelNinjaChocado(ninja)] = null;
				}
				rasengan = null;
				ninjasAsesinados += 1;
			}
		}
		
		// Nuevo ninja
		for (int i = 0; i < ninja.length; i++) {
			double w = entorno.ancho()/4.3; // ninjaXinicial = entorno.ancho/4.3 = 186.04...
			double z = entorno.alto()/4.5; // ninjaYinicial = entorno.alto/4.5 = 133.33...
			if (ninja[i] == null && temporizador[i] > 250) {
				if (i < 2) { // ninjas 0 y 1 verticales
					w = w + entorno.ancho()/1.85 * i;
					ninja[i] = new Ninja(w,entorno.alto(),30,0.6);
					temporizador[i] = 0;
				} 
				if (i >= 2) { // ninjas 2 y 3 horizontales
					z = z + entorno.alto()/1.79 * (i - 2);
					ninja[i] = new Ninja(entorno.ancho(),z,30,0.6);
					temporizador[i] = 0;
				}
			}
		}
		
		// Elige una casa aleatoria para que sea CasaDelPedido
		if (casaDelPedido == null) {
			if (aleatorio.nextInt(2) == 1) {
				casaDelPedido = casas1[aleatorio.nextInt(4)][aleatorio.nextInt(4)];
			} else {
				casaDelPedido = casas2[aleatorio.nextInt(4)][aleatorio.nextInt(4)];
			}
		}
		
		// Dibuja una flecha en la casaDelPedido
		if (casaDelPedido != null) {
			entorno.dibujarImagen(flecha, casaDelPedido.getX() - 40, casaDelPedido.getY(), 0, 0.1);
		}
		
		// Elimina la casa en la que hizo la entrega y suma puntaje
		if (sakura.llegasteALaCasa(casaDelPedido)) {
			casaDelPedido = null;
			puntaje += 5;
		}
		
		// Sakura toca moneda y contador puntaje
		if (moneda != null) {
			if (ninjasAsesinados >= 3) {
				if (sakura.chocasteConUnaMoneda(moneda)) {
					Herramientas.play("sonidoMoneda.wav");
					if (sakura.dameElIDeLaMonedaTomada(moneda)==0) { 
						moneda[sakura.dameElIDeLaMonedaTomada(moneda)] = null;
					}
					if (sakura.dameElIDeLaMonedaTomada(moneda)==1) { 
						moneda[sakura.dameElIDeLaMonedaTomada(moneda)] = null;
					}
					puntaje += 1;
				}
			}
		}
		
		// Volver a crear moneda
		for (int i=0; i<moneda.length; i++) {
			double f = entorno.ancho()/6  + entorno.ancho()/2* i;
			double g = entorno.alto()/4.3 + entorno.alto()/1.83 * i;
			if (moneda[i] == null && temporizador[i] > 500) {
				if (i<2) {
					moneda[i] = new Moneda(f,g,30,Color.YELLOW);
					temporizador[i] = 0;
				}
			}
		}
		
		// Shurikens Mover
		for (int i = 0; i < shurikens.length ; i++) {
			if(shurikens[i]!=null) {
				shurikens[i].mover();
			}
		}
		
		// Shurikens limite con entorno
		for(int i = 0; i < shurikens.length; i++) {
			if(shurikens[i]!=null && shurikens[i].chocoConElEntorno(entorno))
				shurikens[i] = null;
		}
		
		// Volver a crear Shurikens
		for(int i = 0; i < shurikens.length; i++) {
			if(shurikensTemporizador > 100 && shurikens[i] == null && ninja[i]!=null) {
				shurikens[i] = new Shuriken(ninja[i].getX(), ninja[i].getY(), 15, 3, i);
				shurikensTemporizador = 0;
			}
		}
		
		// Victoria
		if (puntaje > 30 && juegoTemp > 250) {
			soundtrack.stop();
			temaVictoria.loop(1);
			sakura.para();
			for (int i=0; i < ninja.length; i++) {
				if (ninja[i] != null) {
					ninja[i].stop();
				}
			}
			
			for (int i = 0; i < shurikens.length; i++) {
				if (shurikens[i] !=null) {
					shurikens[i].stop();
				}
			}
			
			for (int i=0; i<moneda.length; i++) {
				if (moneda[i] == null) {
					temporizador[i] = 0;
				}
			}
			entorno.dibujarImagen(finalMarco, entorno.ancho()/2, entorno.alto()/2, 0, 1.5);
			entorno.cambiarFont("Pixel Emulator",50,Color.RED);
			entorno.escribirTexto("Ganaste el juego:", 100, entorno.alto()/2);
			entorno.cambiarFont("Pixel Emulator",25,Color.RED);
			entorno.escribirTexto("Sakura entregó todos los paquetes.", 90, entorno.alto()/2 + 50);
		}
		
		if (juegoTemp < 900) {
			entorno.dibujarImagen(introMarco,entorno.ancho()/2,entorno.alto()/1.8,0,0.50);
		}
		
		// Fuente, tamaño y color de las letras para el interfaz
		entorno.cambiarFont("Pixel Emulator", 23, Color.WHITE);
		
		// Interfaz: ninjas asesinados, puntaje y tiempo
		ninjasAsesinadosMSJ = String.valueOf(ninjasAsesinados);
		entorno.dibujarImagen(marco,entorno.ancho()/5.8,entorno.alto()/15.8,0,0.52);
		entorno.escribirTexto("NJS VENCIDOS", entorno.ancho()/35, entorno.alto()/19);
		entorno.escribirTexto(ninjasAsesinadosMSJ, entorno.ancho()/7, entorno.alto()/11);
		
		puntajeMSJ = "Puntaje: " +  String.valueOf(puntaje);
		entorno.dibujarImagen(marco,entorno.ancho()/1.17,entorno.alto()/15.8,0,0.52);
		entorno.escribirTexto(puntajeMSJ, entorno.ancho()-200, entorno.alto()/17);
		
		tiempoMSJ = "Tiempo: " + String.valueOf(juegoTemp / 100);
		entorno.dibujarImagen(marco,entorno.ancho()/1.95,entorno.alto()/15.8,0,0.52);
		entorno.escribirTexto(tiempoMSJ, entorno.ancho()/2.40, entorno.alto()/17);
	}

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Juego juego = new Juego();
	}

}
