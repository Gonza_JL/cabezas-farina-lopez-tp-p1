package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Moneda {
	
	private double x;
	private double y;
	private Image monedaGif;
	
	public Moneda(double x, double y, double tamaño, Color color) {
		this.x = x;
		this.y = y;
		this.monedaGif = Herramientas.cargarImagen("moneda.gif");
	}
	
	public void dibujar(Entorno e) {
		e.dibujarImagen(monedaGif, x, y, 0, 0.15);
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

}
