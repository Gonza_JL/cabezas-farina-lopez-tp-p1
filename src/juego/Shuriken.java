package juego;
import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

public class Shuriken {

	private double x;
	private double y;
	private double tamaño;
	private double velocidad;
	private int direccion;
	private Image shurikenPng;
	
	public Shuriken(double x, double y, double tamaño, double velocidad, int direccion) {
		this.x = x;
		this.y = y;
		this.tamaño = tamaño;
		this.velocidad = velocidad;
		this.direccion = direccion;
		this.shurikenPng = Herramientas.cargarImagen("shuriken.png");
	}

	public void dibujar(Entorno e) {
		e.dibujarImagen(shurikenPng, x, y, 0, 0.06);
	}
	
	public boolean chocoConElEntorno(Entorno e) {
		return x < tamaño / 2 || x > e.ancho() - tamaño / 2 ||
				y < tamaño / 2 || y > e.alto() - tamaño / 2;
	}
	
	public void moverIzquierda() {
		x -= velocidad;
	}

	public void moverDerecha() {
		x += velocidad;
	}
	
	public void moverArriba() {
		y -= velocidad;
	}
	
	public void moverAbajo() {
		y += velocidad;
	}

	public void mover() {
		if(this.direccion == 0) {
			this.moverAbajo();
		}
		if(this.direccion == 1) {
			this.moverArriba();
		}
		if(this.direccion == 2) {
			this.moverDerecha();
		}
		if(this.direccion == 3) {
			this.moverIzquierda();
		}
	}
	
	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getTamaño() {
		return tamaño;
	}
	
	public void stop() {
		this.velocidad = 0;	
	}
	
}
